import "bootstrap/dist/css/bootstrap.css";
import "../css/style.css";

import authUI from "./config/auth.config";
import regUI from "./config/reg.config";

import { validate } from "./helpers/validate";
import { showInputError, removeInputError } from "./views/form";
import { login, reg } from "./services/auth.service";
import { notify } from "./views/notifications";
import { tabs } from "./views/tabs";
import { getNews } from "./services/news.service";

//* Auth UI
const { authForm, authEmail, authPassword } = authUI;
const authInputs = [authEmail, authPassword];

//* Reg UI
const {
  regForm,
  regEmail,
  regPassword,
  regNickname,
  regName,
  regLastName,
  regPhone,
  regCity,
  regCountry,
  regDate,
  regMale,
} = regUI;

const regInputs = [
  regEmail,
  regPassword,
  regNickname,
  regName,
  regLastName,
  regPhone,
  regCity,
  regCountry,
  regDate,
];

document.addEventListener("DOMContentLoaded", () => {
  //* Tabs
  tabs(".js-tabs");

  //* Auth
  authForm.addEventListener("submit", onSubmitAuth);
  authInputs.forEach((el) => {
    el.addEventListener("focus", () => removeInputError(el));
  });

  async function onSubmitAuth(e) {
    e.preventDefault();

    let isValidForm = true;

    authInputs.forEach((el) => {
      const isValidInput = validate(el);
      if (!isValidInput) {
        isValidForm = false;
        showInputError(el);
      }
    });

    if (!isValidForm) return;

    try {
      await login(authEmail.value, authPassword.value);
      await getNews();
      authform.reset();
      notify({
        msg: "Успешно авторизированы",
        className: "alert-success",
        timeout: 5000,
      });
    } catch (err) {
      notify({
        msg: "Ошибка авторизации",
        className: "alert-danger",
        timeout: 5000,
      });
    }
  }

  //* Reg
  regForm.addEventListener("submit", onSubmitReg);
  regInputs.forEach((el) => {
    el.addEventListener("focus", () => removeInputError(el));
  });

  async function onSubmitReg(e) {
    e.preventDefault();

    let isValidForm = true;
    let chooseValueGender = "";

    regInputs.forEach((el) => {
      const isValidInput = validate(el);
      if (!isValidInput) {
        isValidForm = false;
        showInputError(el);
      }
    });

    regMale.forEach((el) => {
      if (el.checked) {
        chooseValueGender = el.value;
      }
      return chooseValueGender;
    });

    let [
      date_of_birth_year,
      date_of_birth_month,
      date_of_birth_day,
    ] = regDate.value.split("-");

    if (!isValidForm) return;

    try {
      await reg(
        regEmail.value,
        regPassword.value,
        regNickname.value,
        regName.value,
        regLastName.value,
        regPhone.value,
        chooseValueGender,
        regCity.options[regCity.selectedIndex].text,
        regCountry.options[regCountry.selectedIndex].text,
        date_of_birth_day,
        date_of_birth_month,
        date_of_birth_year,
      );
      notify({
        msg: "Регистрация прошла успешно",
        className: "alert-success",
        timeout: 5000,
      });
    } catch (err) {
      notify({
        msg: "Ошибка регистрации",
        className: "alert-danger",
        timeout: 5000,
      });
    }
  }
});
