const regUI = {
  regForm: document.forms["regForm"],
  regEmail: document.getElementById("reg-email"),
  regPassword: document.getElementById("reg-password"),
  regNickname: document.getElementById("reg-nickname"),
  regName: document.getElementById("reg-name"),
  regLastName: document.getElementById("reg-last-name"),
  regMale: document.getElementsByName("male"),
  regPhone: document.getElementById("reg-phone"),
  regCity: document.getElementById("reg-сity"),
  regCountry: document.getElementById("reg-country"),
  regDate: document.getElementById("reg-date"),
};

export default regUI;
