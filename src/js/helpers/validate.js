import errorList from "./errorsList.json";

const regExpDic = {
  email: /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/,
  password: /^[0-9a-zA-Z]{4,}$/,
  phone: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/,
  date: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
};

/**
 * Function validate. Check Input on RegExp provided in regExpDic by input data-requred type
 * @param {HTMLInputElement} el
 * @returns {Boolean} - Return true if input valid or doesn`t has data-reqired attr
 */

export function validate(el) {
  const regExpName = el.dataset.required;
  if (el.value.length === 0 || el.value == 0) {
    el.setAttribute(
      "data-invalid-message",
      errorList[el.getAttribute("data-invalid-empty")],
    );
    return false;
  } else {
    el.setAttribute(
      "data-invalid-message",
      errorList[el.getAttribute("data-invalid-field")],
    );
  }

  if (!regExpDic[regExpName]) return true; //* инпут не требует валидации
  return regExpDic[regExpName].test(el.value);
}
