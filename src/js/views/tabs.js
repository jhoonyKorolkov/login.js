export function tabs(container) {
  const tabsContainer = document.querySelector(container);
  if (tabsContainer) {
    tabsContainer.addEventListener("click", (e) => {
      if (
        e.target.classList.contains("js-tab-link") &&
        !e.target.classList.contains("active")
      ) {
        tabsContainer
          .querySelector(".js-tab-link.active")
          .classList.remove("active");
        tabsContainer
          .querySelector(".js-tab-item.active")
          .classList.remove("active");
        let tabIndex = e.target.getAttribute("data-index");
        e.target.classList.add("active");
        tabsContainer
          .querySelector(`.js-tab-item[data-index="${tabIndex}"]`)
          .classList.add("active");
      }
    });
  }
}
